<?php

foreach ($collection as $object) {
    if ($object instanceof ClassA)
        $object->doA();
    if ($object instanceof ClassB)
        $object->doB();
}


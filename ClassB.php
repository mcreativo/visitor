<?php


class ClassB
{
    public function doCommonAction()
    {
        echo 'ClassB doing common action' . PHP_EOL;
    }

    public function doB()
    {
        echo 'ClassB doing B action' . PHP_EOL;
    }
} 
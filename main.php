<?php

function __autoload($class_name)
{
    include $class_name . '.php';
}

/** @var CommonInterface[] */
$collection = [];

for ($i = 0; $i < 10; $i++) {
    $className = 'Class' . chr(ord('A') + rand(0, 1));
    $collection[] = new $className();
}

include 'Client.php';



<?php


class ClassA implements CommonInterface
{
    public function doCommonAction()
    {
        echo 'ClassA doing common action' . PHP_EOL;
    }

    public function doA()
    {
        echo 'ClassA doing A action' . PHP_EOL;
    }
} 